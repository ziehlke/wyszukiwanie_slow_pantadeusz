import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        String fileName = "src/main/resources/pan-tadeusz.txt";
        Map<String, Integer> map = new HashMap<>();


        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                for (String i : line.toLowerCase().replaceAll("[^a-ząćęńł\\ ]", "").split("\\s+")) add(map, i);
            }

        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        map.remove("");
        LinkedHashMap<String, Integer> sortedMap =
                map.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
//                        .limit(5)
                        .collect(Collectors.toMap(t -> t.getKey(),t -> t.getValue(),  (oldVal, newVal) -> oldVal, LinkedHashMap::new));

        System.out.println("Do napisania tekstu uzyto: " + sortedMap.size() + " slow.\nOto one wg. czestosci wystepowania: ");

        System.out.println(sortedMap);


        long pojedynczeSlowa =
                map.entrySet().stream()
                        .filter(t -> t.getValue() == 1)
                        .count();
        System.out.println("W tekscie jest: " + pojedynczeSlowa + " slow, ktore wystepuja tylko raz.");

    }

    public static void add(Map<String, Integer> map, String word) {
        map.put(word, map.containsKey(word) ? map.get(word) + 1 : 1);
    }
}
